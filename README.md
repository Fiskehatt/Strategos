# Strategos

## Intro:

Strategos is an addon for Vanilla World of Warcraft (1.12).

## Features:

* Enhanced World Map/Battlefied Minimap
    * raid members are colored according to their current health
    * Warsong Gulch friendly flag carry is highlighted
    * Arathi Basin and Alterac Valley capture timers (click to annouce)
    * Arathi Basin 30yd flag indicators
    * Highlight currently targeted raid member
* Warsong Gulch
    * Show flag carriers names near world status (upper part of the screen, click to target)
    * Flag cariers low health warning in BattleGround chat

## Thanks to:

Iowan
