
function RingFrame_SetTimer(this, start, duration, reverse)
    if ( start > 0 and duration > 0) then
        this.start = start;
        this.duration = duration;
        this.stopping = false;
        this.reverse = reverse
        this:Show();
    else
        this:Hide();
    end
end

function RingFrame_SetColor(this, rgba)
    local _,f = this:GetRegions()
    f:SetVertexColor(unpack(rgba))
end

local function rotate(this, r)
    function c(x,y)
        local sin = math.sin(r)
        local cos = math.cos(r)
        return (x*cos - y * sin)/2+0.5, -(x*sin + y * cos)/2 + 0.5
    end
    local ulx, uly = c(-1,1)
    local llx, lly = c(-1,-1)
    local urx, ury = c(1,1)
    local lrx, lry = c(1,-1)
    this:SetTexCoord(
        ulx,uly,
        llx,lly,
        urx,ury,
        lrx,lry)
end

function RingFrame_OnUpdate()
    if this.stopping ~= true then
        local finished = (GetTime() - this.start) / this.duration;
        if ( finished <= 1.0 ) then
                local r = finished *math.pi*2;
                rotate(this:GetRegions(),this.reverse and r or -r)
                return;
        end
        this.stopping = true
        this:Hide()
    else
    end
end
