function BaseFrame_Assault(this, start, time, faction)
    RingFrame_SetTimer(this.ring, start, time, true)
    if faction == 0 then
        RingFrame_SetColor(this.ring,{0,0,1})
    else
        RingFrame_SetColor(this.ring,{1,0,0})
    end
end
