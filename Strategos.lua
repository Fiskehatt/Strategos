Strategos_EventHandler = CreateFrame("FRAME")
Strategos_EventHandler:RegisterEvent("ADDON_LOADED")

local Strategos_EventList = {
    "WORLD_MAP_UPDATE",
    "CHAT_MSG_BG_SYSTEM_NEUTRAL",
    "CHAT_MSG_BG_SYSTEM_ALLIANCE",
    "CHAT_MSG_BG_SYSTEM_HORDE",
    "CHAT_MSG_MONSTER_YELL",
    "PLAYER_ENTERING_WORLD",
    "UPDATE_WORLD_STATES"
}

local LOCALE = {
    warsong = {
        zoneName = "Warsong Gulch",
        pick = "The %w* [Ff]lag was picked up by (%w*)!",
        drop = "The %w* [Ff]lag was dropped by (%w*)!",
        capture = "(%w*) captured the %w*"
    },
    arathi = {
        zoneName = "Arathi Basin",
        assault = "has assaulted the ([%a%s]+)",
        taken = "has taken the ([%a%s]+)",
        defend = "has defended the ([%a%s]+)",
        claim = "claims the ([%a%s]+)",
        ann = {
            gm = "mine",
            bs = "blacksmith",
            lm = "lumber mill",
            st = "stables",
            fm = "farm"
        },
        poi = {
            gm = "Gold Mine",
            fm = "Farm",
            st = "Stables",
            lm = "Lumber Mill",
            bs = "Blacksmith"
        }
    },
    alterac = {
        zoneName = "Alterac Valley",
        assault = "The (.*) is under attack",
        defend = "The (.*) was taken by",
        ann = {
            wft = "West Frostwolf Tower",
            eft = "East Frostwolf Tower",
            it = "Iceblood Tower",
            tp = "Tower Point",
            ig = "Iceblood Graveyard",
            fg = "Frostwolf Graveyard",
            frh = "Frostwolf Relief Hut",
            dbnb = "Dun Baldar North Bunker",
            dbsb = "Dun Baldar South Bunker",
            ib = "Icewing Bunker",
            sb = "Stonehearth Bunker",
            sas = "Stormpike Aid Station",
            spg = "Stormpike Graveyard",
            shg = "Stonehearth Graveyard",
            sg = "Snowfall Graveyard"
        },
        poi = {
            wft = "West Frostwolf Tower",
            eft = "East Frostwolf Tower",
            it = "Iceblood Tower",
            tp = "Tower Point",
            ig = "Iceblood Graveyard",
            fg = "Frostwolf Graveyard",
            frh = "Frostwolf Relief Hut",
            dbnb = "Dun Baldar North Bunker",
            dbsb = "Dun Baldar South Bunker",
            ib = "Icewing Bunker",
            sb = "Stonehearth Bunker",
            sas = "Stormpike Aid Station",
            spg = "Stormpike Graveyard",
            shg = "Stonehearth Graveyard",
            sg = "Snowfall Graveyard"
        }
    }
}

local function toString(a)
  if a == nil then return "nil" end
  if a == true then return "TRUE" end
  if a == false then return "FALSE" end
  if type(a) == "string" then return "\""..a.."\"" end
  if type(a) == "table" then
      local out
      out = "{"
      for k,v in a do
          out = out..(out~="{"and", "or"")..toString(k)..": "..toString(v)
      end
      return out.."}"
    end
    return a
end
local function debug(a)
  DEFAULT_CHAT_FRAME:AddMessage(toString(a))
end

local function tableini(t,d)
    for k,v in d do
        if type(t[k]) == 'table' then
            tableini(t[k],v)
        end
    end
end

local DEFAULTS = {
    bgs = {
        arathi = {
            distance = 0.0168,
            deltas = 0.00005,
            mapName = "ArathiBasin",
            bases = {
                lm = {
                    x = 0.4039658159017563,
                    y = 0.5570576936006546
                },
                st = {
                    x = 0.3747319206595421,
                    y = 0.2917306870222092
                },
                gm = {
                    x = 0.5751497000455856,
                    y = 0.3086424916982651
                },
                fm = {
                    x = 0.5603127181529999,
                    y = 0.5996280610561371
                },
                bs = {
                    x = 0.4622423946857452,
                    y = 0.4537641629576683
                }
            }
        },
        warsong = {
            distance = 0,
            deltas = 0,
            bases = {}
        },
        alterac = {
            disance = 0,
            deltas = 0,
            bases = {
                dbnb = { x = 0.1, y = 0.2 },
                dbsb = { x = 0.1, y = 0.4 },
                ib   = { x = 0.1, y = 0.6 },
                sb   = { x = 0.1, y = 0.8 },
                sas  = { x = 0.2, y = 0.2 },
                spg  = { x = 0.2, y = 0.4 },
                shg  = { x = 0.2, y = 0.6 },
                sg   = { x = 0.2, y = 0.8 },
                it   = { x = 0.9, y = 0.2 },
                tp   = { x = 0.9, y = 0.4 },
                eft  = { x = 0.9, y = 0.8 },
                wft  = { x = 0.9, y = 0.6 },
                ig   = { x = 0.8, y = 0.2 },
                fg   = { x = 0.8, y = 0.4 },
                frh  = { x = 0.8, y = 0.6 }
            }
        }
    }
}

Strategos_Session = {}

local function split(s,p)
    local r = {}
    for k in string.gfind(s,p) do
        table.insert(r,k)
    end
    return r
end

local function resolve(t,...)
    local r = t
    for i = 1,arg.n do
        r = r[arg[i]]
        if r == nil then return nil end
    end
    return r
end

local function join(t1,t2)
    if type(t2) == "table" then
        if t1 == nil then t1 = {} end
        local r = {}
        for k,v in t1 do
            r[k] = join(nil,v)
        end
        for k,v in t2 do
            r[k] = join(r[k],v)
        end
        return r
    elseif type(t1) == "table" then
        return join(nil,t1)
    else
        return t2 or t1
    end
end

local function get(...)
    local r = resolve(Strategos_Session, unpack(arg))
    local r2 = resolve(Strategos, unpack(arg))
    local r3 = resolve(DEFAULTS, unpack(arg))
    return join(join(r3,r2),r)
end

local function set(v,...)
    local r = Strategos_Session
    for i = 1,arg.n -1 do
        local s = arg[i]
        if r[s] == nil then r[s] = {} end
        r = r[s]
    end
    r[arg[arg.n]] = v
end


local function save(v,...)
    local r = Strategos
    for i = 1,arg.n -1 do
        local s = arg[i]
        if r[s] == nil then r[s] = {} end
        r = r[s]
    end
    r[arg[arg.n]] = v
    set(v,unpack(arg))
end

function Strategos_RegisterEvents()
    for _,e in Strategos_EventList do
        Strategos_EventHandler:RegisterEvent(e)
    end
end

function Strategos_UnregisterEvents()
    for _,e in Strategos_EventList do
        Strategos_EventHandler:UnregisterEvent(e)
    end
end

local function dist(x0,y0,x1,y1)
    return math.sqrt(math.pow(x0-x1,2) + math.pow(y0-y1,2))
end

local function topx(x,y)
    return x, y*BattlefieldMinimap:GetHeight()/BattlefieldMinimap:GetWidth()
end

function Strategos_EventHandler.ADDON_LOADED()
    if arg1 == "Strategos" then
        if Strategos == nil then Strategos = {} end
        if Strategos.dataVersion == nil then
            Strategos = {}
            Strategos.dataVersion = 0.1
        end
        Strategos_BuffScanner = CreateFrame("GameTooltip","StrategosBuffScanner",UIParent,"GameTooltipTemplate")
        Strategos_RegisterEvents()
        Strategos_EventHandler:SetScript("OnUpdate",
            function ()
                if GetRealZoneText() ~= LOCALE.arathi.zoneName then return end
                local xp, yp = topx(GetPlayerMapPosition("player"))
                local pdd = 0
                if get("player") then 
                    pdd = dist(xp,yp,get("player","x"),get("player","y"))
                    if pdd ~= 0 then
                        save(get("bgs","arathi","deltas") * 0.999 + pdd * 0.001, "bgs","arathi","deltas")
                    end
                end
                for i = 1,GetNumRaidMembers() do
                    local x,y = topx(GetPlayerMapPosition("raid"..i))
                    set({x = xp, y = yp},"player")
                    local d = dist(x,y,xp,yp)
                    local rm = get("raid",i)
                    if rm then
                        local dd = dist(x,y,rm.x,rm.y)
                        --if rm.c ~= CheckInteractDistance("raid"..i,4) then debug("d: " .. dd .. "/".. Strategos.bgs.arathi.deltas * 10) end
                        local sd = get("bgs","arathi","deltas")
                        if rm.c ~= CheckInteractDistance("raid"..i,4) and dd < sd * 10 and pdd < sd * 10 and UnitIsVisible("raid"..i,4) and rm.v then
                            save(get("bgs","arathi","distance") * 0.99 + 0.01 * (rm.d + d)/2,"bgs","arathi","distance")
                            --debug(Strategos.bgs.arathi.distance)
                            --debug((rm.d + d)/2)
                        end
                    else
                        rm = {}
                    end
                    rm.d = d
                    rm.x = x
                    rm.y = y
                    rm.c = CheckInteractDistance("raid"..i,4)
                    rm.v = UnitIsVisible("raid"..i,4)
                    set(rm,"raid",i)
                end
            end
        )
                            
        
        Strategos_EventHandler:UnregisterEvent("ADDON_LOADED")
        
        DEFAULT_CHAT_FRAME:AddMessage("|cffffff88Strategos|r loaded.")
        
    end
end

function Strategos_EventHandler.CHAT_MSG_BG_SYSTEM_NEUTRAL()
    Strategos_BGMessage(0)
end

function Strategos_EventHandler.CHAT_MSG_BG_SYSTEM_ALLIANCE()
    Strategos_BGMessage(1)
end

function Strategos_EventHandler.CHAT_MSG_BG_SYSTEM_HORDE()
    Strategos_BGMessage(2)
end

function Strategos_EventHandler.CHAT_MSG_MONSTER_YELL()
    if strfind(arg1, "Alliance") then
        Strategos_BGMessage(1)
    elseif strfind(arg1, "Horde") then
        Strategos_BGMessage(2)
    end
end

function Strategos_BGMessage(faction)
    local function lookup(base)
        for k,_ in get("bgs","arathi","bases") do
            local s = strupper(k)
            if base == LOCALE.arathi.ann[k] then
                return s, k
            end
        end
        for k,_ in get("bgs","alterac","bases") do
            local s = strupper(k)
            if base == LOCALE.alterac.ann[k] then
                return s, k
            end
        end
    end
    local function startTimer(bg, b, d, f)
        local t = get("bgs",bg,"bases",b,"timer") or {}
        t.start = GetTime()
        t.duration = d
        t.reverse = true
        t.faction = faction -1
        set(t,"bgs",bg,"bases",b,"timer")
    end
    
    local _,_,base = strfind(arg1,LOCALE.arathi.assault)
    if base then
        local s, b = lookup(base)
        local f = getglobal("BattlefieldMinimap"..s)
        if f then BaseFrame_Assault(f, GetTime(), 60,faction -1) end
        BaseFrame_Assault(getglobal("WorldMapButton"..s),GetTime() ,60,faction -1)
        startTimer("arathi",b,60)
    end
    local _,_,base = strfind(arg1,LOCALE.arathi.claim)
    if base then
        local s, b = lookup(base)
        local f = getglobal("BattlefieldMinimap"..s)
        if f then BaseFrame_Assault(f, GetTime(), 60,faction -1) end
        BaseFrame_Assault(getglobal("WorldMapButton"..s),GetTime() ,60,faction -1)
        startTimer("arathi",b,60)
    end
    local _,_,base = strfind(arg1,LOCALE.arathi.defend)
    if base then
        local s, b = lookup(base)
        local f = getglobal("BattlefieldMinimap"..s)
        if f then BaseFrame_Assault(f, GetTime(), 0,faction -1) end
        BaseFrame_Assault(getglobal("WorldMapButton"..s),GetTime() ,0,faction -1)
        startTimer("arathi",b,0)
    end
    local _,_,base = strfind(arg1,LOCALE.alterac.assault)
    if base then
        local s, b = lookup(base)
        local f = getglobal("BattlefieldMinimap"..s)
        if f then BaseFrame_Assault(f, GetTime(), 300,faction -1) end
        BaseFrame_Assault(getglobal("WorldMapButton"..s),GetTime() ,300,faction -1)
        startTimer("alterac",b,300)
    end
    local _,_,base = strfind(arg1,LOCALE.alterac.defend)
    if base then
        local s, b = lookup(base)
        local f = getglobal("BattlefieldMinimap"..s)
        if f then BaseFrame_Assault(f, GetTime(), 0,faction -1) end
        BaseFrame_Assault(getglobal("WorldMapButton"..s),GetTime() ,0,faction -1)
        startTimer("alterac",b,0)
    end
    local _,_,name = strfind(arg1,LOCALE.warsong.pick)
    if name then
        set(name,"bgs","warsong",faction == 1 and "afc" or "hfc")
        if faction == 1 then
            StrategosAlly:SetText(name)
            StrategosAlly.lastWarn = nil
            StrategosAlly:Show()
        else
            StrategosHorde:SetText(name)
            StrategosHorde.lastWarn = nil
            StrategosHorde:Show()
        end
    end
    local _,_,name = strfind(arg1,LOCALE.warsong.drop)
    if name then
        set("","bgs","warsong",faction ~= 1 and "afc" or "hfc")
        StrategosAlly:SetText("")
    end
    local _,_,name = strfind(arg1,LOCALE.warsong.capture)
    if name then
        set("","bgs","warsong",faction == 1 and "afc" or "hfc")
        StrategosHorde:SetText("")
    end
    
end

function Strategos_HasWSGFlag(unit)
    for i =1,16 do
        Strategos_BuffScanner:SetOwner(UIParent, "ANCHOR_NONE");
        Strategos_BuffScanner:SetUnitBuff(unit,i)
        local sn = getglobal("StrategosBuffScannerTextLeft1"):GetText()
        if sn == "Warsong Flag" then
            Strategos_BuffScanner:Hide()
            return true
        end
    end
    Strategos_BuffScanner:Hide()
end

function Strategos_EventHandler.WORLD_MAP_UPDATE()
    local function announce(name)
        if this.stopping ~= true then
            local t = math.ceil(this.duration - (GetTime() - this.start))
            local s = math.mod(t,60)
            local m = math.floor(t/60)
            SendChatMessage(name .. " - ".. (m == 0 and "" or m .. "m " ) .. s .. "s","BATTLEGROUND")
        end
    end
    local zone = GetMapInfo()
    local function decorate(tzone, bg, mapini, minimapini, mapup, minimapup)
        if zone == tzone then
            Strategos_POISScan(bg)
            local fm = getglobal("WorldMapButton")
            if get("bgs",bg,"map") ~= true then
                for b,v in get("bgs",bg,"bases") do
                    mapini(fm,b,v)
                end
                set(true,"bgs",bg,"map")
            else
                for b,v in get("bgs",bg,"bases") do
                    mapup(fm,b,v)
                end
            end
            
            fm = getglobal("BattlefieldMinimap")
            if fm then
                if get("bgs",bg,"minimap") ~= true then
                    for b,v in get("bgs",bg,"bases") do
                        minimapini(fm,b,v)
                    end
                    set(true,"bgs",bg,"minimap")
                else
                    for b,v in get("bgs",bg,"bases") do
                        minimapup(fm,b,v)
                    end
                end
            end
        else
            if get("bgs",bg,"map") then
                for b,_ in get("bgs",bg,"bases") do
                    local f = getglobal("WorldMapButton"..strupper(b))
                    f:Hide()
                end
            end
            if get("bgs",bg,"minimap") then
                for b,_ in get("bgs",bg,"bases") do
                    local f = getglobal("BattlefieldMinimap"..strupper(b))
                    f:Hide()
                end
            end
        end
    end
    decorate("ArathiBasin","arathi",
        function (fm, b, v)
            local f = CreateFrame("frame","WorldMapButton"..strupper(b),fm,"BaseFrameTemplate")
            f.tex = f:GetRegions()
            f.tex:SetAllPoints()
            f.ring:SetScale(2.5)
            f.tex:SetTexture("Interface\\Addons\\Strategos\\Textures\\RangeIndicators.tga")
            f.ring:SetScript("OnClick", function() announce(name) end)
            f:SetPoint("CENTER",fm,"TOPLEFT",v.x*fm:GetWidth(),-v.y*fm:GetHeight())
            local c = get("bgs","arathi","distance")*2*(32/28.5)*(30/28)
            f:SetWidth(c*fm:GetWidth()) f:SetHeight(c*fm:GetWidth())
            local name = GetMapLandmarkInfo(get("bgs","arathi","bases",b,"poi"))
            f.ring:SetScript("OnClick", function() announce(name) end)
            f:Show()
        end,
        function (fm, b, v)
            local f = CreateFrame("frame","BattlefieldMinimap"..strupper(b),fm,"BaseFrameTemplate")
            f:SetPoint("CENTER",fm,"TOPLEFT",v.x*fm:GetWidth(),-v.y*fm:GetHeight())
            local timer = get("bgs","arathi","bases",b,"timer")
            if timer and GetTime() - timer.start < timer.duration then
                BaseFrame_Assault(f, timer.start, timer.duration, timer.faction)
            end
            local name = GetMapLandmarkInfo(get("bgs","arathi","bases",b,"poi"))
            f.ring:SetScript("OnClick", function() announce(name) end)
            f:Show()
        end,
        function (fm, b, v)
            local f = getglobal("WorldMapButton"..strupper(b))
            local c = get("bgs","arathi","distance")*2*(32/28.5)*(30/28)
            f:SetWidth(c*fm:GetWidth()) f:SetHeight(c*fm:GetWidth())
            f:Show()
        end,
        function (fm, b, v)
            local f = getglobal("BattlefieldMinimap"..strupper(b))
            f:Show()
        end
    )

    decorate("AlteracValley","alterac",
        function (fm, b, v)
            local f = CreateFrame("frame","WorldMapButton"..strupper(b),fm,"BaseFrameTemplate")
            f:SetPoint("CENTER",fm,"TOPLEFT",v.x*fm:GetWidth()/2.5,-v.y*fm:GetHeight()/2.5)
            local tex = getglobal("WorldMapButton"..strupper(b).."Pin")
            tex:SetTexture("Interface\\Minimap\\POIIcons")
            local name, _, textureIndex = GetMapLandmarkInfo(get("bgs","alterac","bases",b,"poi"))
            x1, x2, y1, y2 = WorldMap_GetPOITextureCoords(textureIndex)
            tex:SetTexCoord(x1, x2, y1, y2);
            f:SetScale(2.5)
            f.ring:SetScript("OnClick", function() announce(name) end)
            f:Show()
        end,
        function (fm, b, v)
            local f = CreateFrame("frame","BattlefieldMinimap"..strupper(b),fm,"BaseFrameTemplate")
            f:SetPoint("CENTER",fm,"TOPLEFT",v.x*fm:GetWidth(),-v.y*fm:GetHeight())
            local tex = getglobal("BattlefieldMinimap"..strupper(b).."Pin")
            local name, _, textureIndex = GetMapLandmarkInfo(get("bgs","alterac","bases",b,"poi"));
            x1, x2, y1, y2 = WorldMap_GetPOITextureCoords(textureIndex);
            tex:SetTexture("Interface\\Minimap\\POIIcons")
            tex:SetTexCoord(x1, x2, y1, y2);
            f.ring:SetScript("OnClick", function() announce(name) end)
            local timer = get("bgs","alterac","bases",b,"timer")
            if timer and GetTime() - timer.start < timer.duration then
                BaseFrame_Assault(f, timer.start, timer.duration, timer.faction)
            end
            f:Show()
        end,
        function (fm, b, v)
            local f = getglobal("WorldMapButton"..strupper(b))
            local _, _, textureIndex = GetMapLandmarkInfo(get("bgs","alterac","bases",b,"poi"))
            x1, x2, y1, y2 = WorldMap_GetPOITextureCoords(textureIndex)
            getglobal("WorldMapButton"..strupper(b).."Pin"):SetTexCoord(x1, x2, y1, y2)
            f:Show()
        end,
        function (fm, b, v)
            local f = getglobal("BattlefieldMinimap"..strupper(b))
            local _, _, textureIndex = GetMapLandmarkInfo(get("bgs","alterac","bases",b,"poi"));
            x1, x2, y1, y2 = WorldMap_GetPOITextureCoords(textureIndex);
            getglobal("BattlefieldMinimap"..strupper(b).."Pin"):SetTexCoord(x1, x2, y1, y2);
            f:Show()
        end
    )
    
    local function dotUpdate(f, i)
        local unit = "raid"..i
        local p = UnitHealth(unit)/UnitHealthMax(unit)
        local function colorize(t)
            if UnitIsGhost(unit) then
                t:SetVertexColor(0.3,0.3,1)
            elseif UnitIsDead(unit) then
                t:SetVertexColor(0.7,0.7,1)
            else
                t:SetVertexColor((1 - p)*2,p*2,0)
            end
        end
        if UnitIsUnit("target",unit) then
            colorize(f.texTarget)
            if f.texbg then f.texbg:Hide() end
            f.tex:Hide()
            f.texTarget:Show()
        else
            colorize(f.tex)
            if f.texbg then colorize(f.texbg) f.texbg:Show() end
            f.tex:Show()
            f.texTarget:Hide()
        end
        local faction = UnitFactionGroup("player")=="Alliance" and 1 or 2
        if UnitName(unit) == get("bgs","warsong",faction == 1 and "afc" or "hfc") then
            f.texFlag:SetVertexColor((1 - p)*2,p*2,0)
            f.texFlag:GetParent():SetAlpha(1)
            f.texFlag:Show()
        else
            f:SetAlpha(0.50 + p*0.50)
            f.texFlag:Hide()
        end
    end
    for i=1,40 do
        local n = i
        local f=getglobal("BattlefieldMinimapRaid"..i)
        if f and f.tex == nil then
            local fb = f:GetRegions()
            f.tex = f:CreateTexture()
            fb:SetTexture("Interface\\Addons\\Strategos\\Textures\\DotBackground.tga")
            fb:SetDrawLayer("BORDER")
            f.texbg = fb
            f.tex:SetTexture("Interface\\Addons\\Strategos\\Textures\\Dot.tga")
            f.tex:SetDrawLayer("ARTWORK")
            f.tex:SetAllPoints(f)
            f.texFlag = f:CreateTexture()
            f.texFlag:SetTexture("Interface\\Addons\\Strategos\\Textures\\Star.tga")
            f.texFlag:SetDrawLayer("OVERLAY")
            f.texFlag:SetAllPoints(f)
            f.texTarget = f:CreateTexture()
            f.texTarget:SetTexture("Interface\\Addons\\Strategos\\Textures\\DotHighlight.tga")
            f.texTarget:SetDrawLayer("OVERLAY")
            f.texTarget:SetAllPoints(f)
            f:SetScript("OnUpdate",function() dotUpdate(f,n)end)
        end
        
        local f=getglobal("WorldMapRaid"..i)
        if f and f.tex == nil then
            f:GetRegions():SetDrawLayer("BACKGROUND")
            f.tex = f:CreateTexture()
            f.tex:SetTexture("Interface\\Addons\\Strategos\\Textures\\Circle.tga")
            f.tex:SetDrawLayer("ARTWORK")
            f.tex:SetPoint("CENTER" , f)
            f.tex:SetWidth(f:GetWidth() * 1.5)
            f.tex:SetHeight(f:GetHeight() * 1.5)
            f.texFlag = f:CreateTexture()
            f.texFlag:SetTexture("Interface\\Addons\\Strategos\\Textures\\Star.tga")
            f.texFlag:SetDrawLayer("OVERLAY")
            f.texFlag:SetAllPoints(f)
            f.texTarget = f:CreateTexture()
            f.texTarget:SetTexture("Interface\\Addons\\Strategos\\Textures\\DotHighlight.tga")
            f.texTarget:SetDrawLayer("OVERLAY")
            f.texTarget:SetAllPoints(f)
            f:SetScript("OnUpdate",function() dotUpdate(f,n)end)
        end
    end
end
local function wsgInit()
    local faction = UnitFactionGroup("player")=="Alliance" and 1 or 2
    if StrategosAlly == nil then
        local function click()
            local t = this:GetText()
            if t and t ~= "" then
                TargetByName(t,1)
            end
        end
        local function update(faction)
            local function setAndWarn(o,p)
                o:SetTextColor((1 - p)*2,p*2,0)
                local timeout = UnitFactionGroup("player") == faction and 30 or 10
                if o.lastWarn == nil or GetTime() - o.lastWarn > timeout then
                    o.lastP = 1
                end
                if o.lastP > p then
                    for _,pp in {0.10,0.20,0.40} do
                        if p < pp and (o.lastP == nil or o.lastP > pp) then
                            SendChatMessage(faction .. " Flag Carrier is below "..(pp*100).."% Health!","BATTLEGROUND")
                            o.lastWarn = GetTime()
                            o.lastP = p
                            break
                        end
                    end
                end
            end
            local t = this:GetText()
            if (t == nil or t == "") and UnitFactionGroup("player") == faction and (this.lastUpdate == nil or GetTime() - this.lastUpdate > 1.5) then
                this.lastUpdate = GetTime()
                for i = 1,GetNumRaidMembers() do
                    local r = "raid"..i
                    if Strategos_HasWSGFlag(r) then
                        local name = UnitName(r)
                        set(name,"bgs","warsong",faction == "Alliance" and "afc" or "hfc")
                        if faction == "Alliance" then
                            StrategosAlly:SetText(name)
                            StrategosAlly:Show()
                        else
                            StrategosHorde:SetText(name)
                            StrategosHorde:Show()
                        end
                        break
                    end
                end
            end
            if t and t ~= "" then
                if UnitFactionGroup("player") == faction then
                    for i = 1,GetNumRaidMembers() do
                        local r = "raid"..i
                        if UnitName(r) == t then
                            local p = UnitHealth(r)/UnitHealthMax(r)
                            setAndWarn(this,p)
                            return
                        end
                    end
                else
                    if UnitName(this.last or "") == t then
                        local p = UnitHealth(this.last)/UnitHealthMax(this.last)
                        setAndWarn(this,p)
                    elseif this.lastUpdate == nil or GetTime() - this.lastUpdate > 0.5 then
                        this.lastUpdate = GetTime()
                        for i = 1,GetNumRaidMembers() do
                            local r = "raid"..i.."target"
                            if UnitIsPlayer(r) and UnitName(r) == t then
                                local p = UnitHealth(r)/UnitHealthMax(r)
                                setAndWarn(this,p)
                                this.last = r
                                return
                            end
                        end
                        this:SetTextColor(1,1,1)
                    end
                end
            end
        end
        local f = CreateFrame("Button","StrategosAlly",AlwaysUpFrame1DynamicIconButton)
        f:SetText(get("bgs","warsong","afc") or "")
        f:SetPoint("LEFT",AlwaysUpFrame1DynamicIconButton,"RIGHT")
        f:SetHeight(24)
        f:SetWidth(100)
        f:SetTextFontObject("GameFontHighlight")
        f:SetBackdrop({bgFile="Interface\\ChatFrame\\ChatFrameBackground"})
        f:SetBackdropColor(0,0,1,0.25)
        f:SetScript("OnClick",click)
        f:SetScript("OnUpdate",function() update("Alliance") end)
        local f = CreateFrame("Button","StrategosHorde",AlwaysUpFrame2DynamicIconButton)
        f:SetText(get("bgs","warsong","hfc") or "")
        f:SetPoint("LEFT",AlwaysUpFrame2DynamicIconButton,"RIGHT")
        f:SetHeight(24)
        f:SetWidth(100)
        f:SetTextFontObject("GameFontHighlight")
        f:SetScript("OnClick",click)
        f:SetScript("OnUpdate",function() update("Horde") end)
        f:SetBackdrop({bgFile="Interface\\ChatFrame\\ChatFrameBackground"})
        f:SetBackdropColor(1,0,0,0.25)
    end
    StrategosAlly:Show()
    StrategosHorde:Show()
end

 
function Strategos_EventHandler.PLAYER_ENTERING_WORLD()
    local faction = UnitFactionGroup("player")=="Alliance" and 1 or 2
    if GetRealZoneText() == LOCALE.warsong.zoneName then
        set("","bgs","warsong","afc")
        set("","bgs","warsong","hfc")
        if get("bgs","warsong",faction == 1 and "afc" or "hfc") == nil and GetWorldStateUIInfo(faction) == 2 then
            for i = 1,GetNumRaidMembers() do
                if Strategos_HasWSGFlag("raid"..i) then
                    set(UnitName("raid"..i),"bgs","warsong",faction == 1 and "afc" or "hfc")
                    break
                end
            end
        end
        wsgInit()
    end
end

function Strategos_EventHandler.UPDATE_WORLD_STATES()
    local faction = UnitFactionGroup("player")=="Alliance" and 1 or 2
    if GetRealZoneText() == LOCALE.warsong.zoneName then
        wsgInit()
    else
        set("","bgs","warsong",faction == 1 and "afc" or "hfc")
        if StrategosAlly then
            StrategosAlly:Hide()
            StrategosHorde:Hide()
        end
    end
    set("","bgs","warsong",faction ~= 1 and "afc" or "hfc")
end

Strategos_EventHandler:SetScript("OnEvent",
    function ()
        if Strategos_EventHandler[event] then
            Strategos_EventHandler[event]()
        end
    end
)

function Strategos_POISScan(bg)
    for i = 1,GetNumMapLandmarks() do
        local name = GetMapLandmarkInfo(i)
        for b,_ in get("bgs",bg,"bases") do
            if name == LOCALE[bg].poi[b] then
                set(i,"bgs",bg,"bases",b,"poi")
            end
        end
    end
end
